Zerofree is a filesystem utitily for filling unused blocks with zeros on a variety of filesystems (btrfs, fat, ntfs,...). This utility based on Partclone source code.

Partclone is a project similar to the well-known backup utility "Partition Image" a.k.a partimage. Partclone provides utilities to back up and restore used-blocks of a partition and it is designed for higher compatibility of the file system by using existing library, e.g. e2fslibs is used to read and write the ext2 partition.

Compile:

    autoreconf -f -i
    ./configure --enable-btrfs --enable-zerofree
    make

Usage:

Zero unused space on an image or device:

    zerofree.btrfs /dev/sda1
    zerofree.fat /dev/sda1
    zerofree.ntfs /dev/sda1


Limitations:

  - Filesystem being backedup must be unmounted and inaccessible to other programs.

For more info about partclone, check our website http://partclone.org or github-wiki.
