#!/bin/bash

set -e

Z=$(git describe --tags --abbrev=0)
V="${Z#*-}"
N="zerofree-partclone"
T="$N-$V"
A=$(dpkg --print-architecture)
M=$USER@$(uname -n)

mkdir -p "$T/DEBIAN"

cat << EOF > "$T/DEBIAN/control"
Package: $N
Maintainer: $M
Version: $V
Architecture: $A
Description: zerofree btrfs, fat, ntfs
EOF

chmod -R 0755 "$T/DEBIAN"

sudo apt build-dep partclone
autoreconf -f -i
./configure --enable-btrfs --enable-fat --enable-ntfs --enable-zerofree
make

B="$T/usr/bin"
mkdir -p "$B"

cp -v ./src/zerofree.btrfs "$B"
cp -v ./src/zerofree.fat "$B"
cp -v ./src/zerofree.ntfs "$B"

dpkg-deb --build --root-owner-group "$T"
