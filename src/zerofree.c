#include <config.h>
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#ifndef _LARGEFILE64_SOURCE
#define _LARGEFILE64_SOURCE
#endif
#include <features.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <sys/ioctl.h>
#include <sys/mount.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <malloc.h>
#include <stdarg.h>
#include <string.h>
#include <getopt.h>
#include <locale.h>
#include <mntent.h>
#include <limits.h>
#include <errno.h>
#include <inttypes.h>
#include <assert.h>
#include "gettext.h"
#include <linux/fs.h>
#include <sys/types.h>
#include <dirent.h>
#define _(STRING) gettext(STRING)
//#define PACKAGE "partclone"
#include "version.h"
#include "partclone.h"
#include "checksum.h"

#include <mcheck.h>
#include <pthread.h>

#include "progress.h"
#include "fs_common.h"

#define APP_NAME "Zerofree"
#define APP_URL "https://gitlab.com/axet/zerofree-partclone"

extern FILE* msg;
extern const char *exec_name;

progress_bar prog;
unsigned long long copied;
unsigned long long block_id;
int done;

/// cmd_opt structure defined in partclone.h
cmd_opt opt;

/// cmd_opt structure defined in partclone.h
fs_cmd_opt fs_opt;

enum {
	OPT_OFFSET_DOMAIN = 1000
};

const char* get_exec_name();

void *thread_update_pui(void *arg);

static void save_program_name(const char* argv0) {
	const char* last_slash = strrchr(argv0, '/');

	if (last_slash != 0) {

		exec_name = last_slash + 1;
	}
}

void print_version_zerofree(void){
	printf(APP_NAME " : v%s (%s) \n", VERSION, git_version);
	exit(0);
}

int open_source_zerofree(char* source, cmd_opt* opt) {
	int ret = 0;
	int debug = opt->debug;
	char *mp = NULL;
	int flags = O_RDWR | O_LARGEFILE;
        struct stat st_dev;
	int ddd_block_device = -1;

	log_mesg(1, 0, 0, debug, "open source file/device %s\n", source);

	if (opt->clone) { /// always is device, clone from device=source

		mp = malloc(PATH_MAX + 1);
		if (!mp)
			log_mesg(0, 1, 1, debug, "%s, %i, not enough memory\n", __func__, __LINE__);

		if (check_mount(source, mp) == 1) {
			log_mesg(0, 0, 1, debug, "device (%s) is mounted at %s\n", source, mp);
			free(mp); mp = NULL;
			log_mesg(0, 1, 1, debug, "error exit\n");
		}
		if (mp){ free(mp); mp = NULL;}

		if ((ret = open(source, flags, S_IRUSR)) == -1)
			log_mesg(0, 1, 1, debug, "clone: open %s error\n", source);

	}

	return ret;
}

void parse_options_zerofree(int argc, char **argv, cmd_opt* opt) {

	static const char *sopt = "hvd::L:cx:brDo:O:s:f:RCFINiqWBz:E:a:k:Kn:Tt";

	static const struct option lopt[] = {
// common options
		{ "help",		no_argument,		NULL,   'h' },
		{ "version",		no_argument,		NULL,   'v' },
		{ "note",		required_argument,	NULL,   'n' },
		{ "source",		required_argument,	NULL,   's' },
		{ "debug",		optional_argument,	NULL,   'd' },
		{ "logfile",		required_argument,	NULL,   'L' },
		{ "UI-fresh",		required_argument,	NULL,   'f' },
		{ "no_check",		no_argument,		NULL,   'C' },
		{ "ignore_crc",		no_argument,		NULL,   'i' },
		{ "force",		no_argument,		NULL,   'F' },
		{ "no_block_detail",	no_argument,		NULL,   'B' },
		{ "buffer_size",	required_argument,	NULL,   'z' },
// not RESTORE and not CHKIMG
#ifndef CHKIMG
#ifndef RESTORE
#ifndef DD
#endif
		{ "checksum-mode",       required_argument, NULL, 'a' },
		{ "blocks-per-checksum", required_argument, NULL, 'k' },
		{ "no-reseed",           no_argument,       NULL, 'K' },
#endif
#endif
// not CHKIMG
#ifndef CHKIMG
		{ "skip_write_error",	no_argument,		NULL,   'w' },
		{ "ignore_fschk",	no_argument,		NULL,   'I' },
		{ "quiet",		no_argument,		NULL,   'q' },
#endif
#ifdef HAVE_LIBNCURSESW
		{ "ncurses",		no_argument,		NULL,   'N' },
#endif
		{ NULL,			0,			NULL,    0  }
	};

	save_program_name(argv[0]);

	int c;
	int mode = 0;
	memset(opt, 0, sizeof(cmd_opt));

	opt->debug = 0;
	opt->offset = 0;
	opt->rescue = 0;
	opt->skip_write_error = 0;
	opt->check = 1;
	opt->ignore_crc = 0;
	opt->quiet = 0;
	opt->no_block_detail = 0;
	opt->fresh = 2;
	opt->logfile = "/var/log/zerofree.log";
	opt->buffer_size = DEFAULT_BUFFER_SIZE;
	opt->checksum_mode = CSM_CRC32;
	opt->reseed_checksum = 1;
	opt->blocks_per_checksum = 0;
	opt->blockfile = 0;

	opt->clone++;
	mode=1;

	int option_index = 0;

	while ((c = getopt_long(argc, argv, sopt, lopt, &option_index)) != -1) {
		switch (c) {
			case 'h':
			case '?':
				usage_zerofree();
				break;
			case 'v':
				print_version_zerofree();
				break;
			case 'n':
				memcpy(opt->note, optarg, NOTE_SIZE);
				break;
			case 'd':
				opt->debug = optarg ? atol(optarg) : 1;
				break;
			case 'L':
				opt->logfile = optarg;
				break;
			case 'f':
				assert(optarg != NULL);
				opt->fresh = atol(optarg);
				break;
			case 'C':
				opt->check = 0;
				break;
			case 'i':
				opt->ignore_crc = 1;
				break;
			case 'F':
				opt->force++;
				break;
			case 'B':
				opt->no_block_detail = 1;
				break;
			case 'z':
				assert(optarg != NULL);
				opt->buffer_size = atol(optarg);
				break;
			case OPT_OFFSET_DOMAIN:
				assert(optarg != NULL);
				opt->offset_domain = (off_t)strtoull(optarg, NULL, 0);
				break;
			case 'R':
				opt->rescue++;
				break;
			case 'a':
				assert(optarg != NULL);
				opt->checksum_mode = convert_to_checksum_mode(atol(optarg));
				break;
			case 'k':
				assert(optarg != NULL);
				opt->blocks_per_checksum = atol(optarg);
				break;
			case 'K':
				opt->reseed_checksum = 0;
				break;
#ifndef CHKIMG
			case 'w':
				opt->skip_write_error = 1;
				break;
			case 'I':
				opt->ignore_fschk++;
				break;
			case 'q':
				opt->quiet = 1;
				break;
			case 'T':
				opt->blockfile = 1;
				break;
			case 't':
				opt->blockfile = 1;
				opt->torrent_only = 1;
				break;
			case 'E':
				assert(optarg != NULL);
				opt->offset = (off_t)atol(optarg);
				break;
#endif
#ifdef HAVE_LIBNCURSESW
			case 'N':
				opt->ncurses = 1;
				break;
#endif
			default:
				fprintf(stderr, "Unknown option '%s'.\n", argv[optind-1]);
				usage_zerofree();
		}
	}

	opt->source = argv[optind++];

	if (mode != 1) {
		usage_zerofree();
	}

	if (!opt->debug) {
		opt->debug = 0;
	}

	if ((!opt->target) && (!opt->source)) {
		fprintf(stderr, "There is no image name. Use --help get more info.\n");
		exit(0);
	}

	if (opt->buffer_size < 512) {
		fprintf(stderr, "Too small or bad buffer size. Use --help get more info.\n");
		exit(0);
	}

	if (opt->offset < 0) {
		fprintf(stderr, "Too small or bad offset. Use --help get more info.\n");
		exit(0);
	}

	if (opt->offset_domain < 0) {
		fprintf(stderr, "Too small or bad offset of domain file. Use --help get more info.\n");
		exit(0);
	}

	if (!opt->source)
		opt->source = "-";

	if (opt->clone || opt->domain) {
		if ((!strcmp(opt->source, "-")) || (!opt->source)) {
			fprintf(stderr, APP_NAME " can't %s from stdin.\nFor help, type: %s -h\n",
				opt->clone ? "clone" : "make domain log",
				get_exec_name());
			exit(0);
		}
	}
}

void usage_zerofree(void) {
	fprintf(stderr, "%s v%s " APP_URL "\nUsage: %s [OPTIONS]\n"
		"    Efficiently zerroing image or device.\n"
		"\n"
		"    -L,  --logfile FILE     Log FILE\n"
		"    -dX, --debug=X          Set the debug level to X = [0|1|2]\n"
		"    -C,  --no_check         Don't check device size and free space\n"
#ifdef HAVE_LIBNCURSESW
		"    -N,  --ncurses          Using Ncurses User Interface\n"
#endif
#ifndef CHKIMG
		"    -I,  --ignore_fschk     Ignore filesystem check\n"
#endif
		"    -i,  --ignore_crc       Ignore checksum error\n"
		"    -F,  --force            Force progress\n"
		"    -f,  --UI-fresh         Fresh times of progress\n"
		"    -B,  --no_block_detail  Show progress message without block detail\n"
		"    -z,  --buffer_size SIZE Read/write buffer size (default: %d)\n"
#ifndef CHKIMG
		"    -q,  --quiet            Disable progress message\n"
#endif
		"    -n,  --note NOTE        Display Message Note (128 words)\n"
		"    -v,  --version          Display partclone version\n"
		"    -h,  --help             Display this help\n"
		, get_exec_name(), VERSION, get_exec_name(), DEFAULT_BUFFER_SIZE);
	exit(0);
}

void print_finish_info_zerofree(cmd_opt opt) {
	int debug = opt.debug;

	setlocale(LC_ALL, "");
	bindtextdomain(PACKAGE, LOCALEDIR);
	textdomain(PACKAGE);
	if (opt.clone)
		log_mesg(0, 0, 1, debug, _(APP_NAME " successfully zerroed the device (%s)\n"), opt.source);
}

void print_partclone_info_zerofree(cmd_opt opt) {
	int debug = opt.debug;

	setlocale(LC_ALL, "");
	bindtextdomain(PACKAGE, LOCALEDIR);
	textdomain(PACKAGE);
	log_mesg(0, 0, 1, debug, _(APP_NAME " v%s " APP_URL "\n"), VERSION);
	if (opt.clone) {
			log_mesg(0, 0, 1, debug, _("Starting to zerroing device (%s)\n"), opt.source);
	}
	if ( strlen(opt.note) > 0 ){
	    opt.note[NOTE_SIZE-1] = '\0';
	    log_mesg(0, 0, 1, debug, "note: %s\n", opt.note);
	}
}

void *thread_update_pui(void *arg) {
	while (!done) {
		if (!opt.quiet)
			update_pui(&prog, copied, block_id, done);
		sleep(opt.fresh);
	}
	pthread_exit("exit");
}

/**
 * main function - for clone or restore data
 */
int main(int argc, char **argv) {
#ifdef MEMTRACE
	setenv("MALLOC_TRACE", "partclone_mtrace.log", 1);
	mtrace();
#endif
	char*			source;			/// source data
	int			dfr;		/// file descriptor for source and target
	int			r_size, w_size;		/// read and write size
	unsigned		cs_size = 0;		/// checksum_size
	int			cs_reseed = 1;
	int			start;
	unsigned long long      stop;		/// start, range, stop number for progress bar
	unsigned long *bitmap = NULL;		/// the point for bitmap data
	int			debug = 0;		/// debug level
	int			tui = 0;		/// text user interface
	int			pui = 0;		/// progress mode(default text)
	
	int			flag;
	int			pres = 0;
	pthread_t		prog_thread;
	void			*p_result;
	struct stat st_dev;

	static const char *const bad_sectors_warning_msg =
		"*************************************************************************\n"
		"* WARNING: The disk has bad sectors. This means physical damage on the  *\n"
		"* disk surface caused by deterioration, manufacturing faults, or        *\n"
		"* another reason. The reliability of the disk may remain stable or      *\n"
		"* degrade quickly. Use the --rescue option to efficiently save as much  *\n"
		"* data as possible!                                                     *\n"
		"*************************************************************************\n";

	file_system_info fs_info;   /// description of the file system
	image_options    img_opt;

	int target_stdout = 0;

	init_fs_info(&fs_info);
	init_image_options(&img_opt);

	/**
	 * get option and assign to opt structure
	 * check parameter and read from argv
	 */
	parse_options_zerofree(argc, argv, &opt);

	/**
	 * if "-d / --debug" given
	 * open debug file in "/var/log/partclone.log" for log message 
	 */
	memset(&fs_opt, 0, sizeof(fs_cmd_opt));
	debug = opt.debug;
	fs_opt.debug = debug;
	fs_opt.ignore_fschk = opt.ignore_fschk;

	//if(opt.debug)
	msg = fopen(opt.logfile,"w");
	if (msg == NULL) {
		msg = fopen("zerofree.log","w");
	}

	/**
	 * using Text User Interface
	 */
	if (opt.ncurses) {
		pui = NCURSES;
		log_mesg(1, 0, 0, debug, "Using Ncurses User Interface mode.\n");
	} else
		pui = TEXT;

	tui = open_pui(pui, opt.fresh);
	if ((opt.ncurses) && (!tui)) {
		opt.ncurses = 0;
		pui = TEXT;
		log_mesg(1, 0, 0, debug, "Open Ncurses User Interface Error.\n");
	}

	/// print partclone info
	print_partclone_info_zerofree(opt);

	/// ignore crc check
	if (opt.ignore_crc)
		log_mesg(1, 0, 1, debug, "Ignore CRC errors\n");

	/**
	 * open source and target
	 * clone mode, source is device and target is image file/stdout
	 * restore mode, source is image file/stdin and target is device
	 * dd mode, source is device and target is device !!not complete
	 */
	source = opt.source;
	log_mesg(1, 0, 0, debug, "source=%s \n", source);
	dfr = open_source_zerofree(source, &opt);
	if (dfr == -1) {
		log_mesg(0, 1, 1, debug, "Error exit\n");
	}

	/**
	 * get partition information like super block, bitmap from device or image file.
	 */
	if (1) {

		log_mesg(1, 0, 0, debug, "Initiate image options - version %s\n", IMAGE_VERSION_CURRENT);

		img_opt.checksum_mode = opt.checksum_mode;
		img_opt.checksum_size = get_checksum_size(opt.checksum_mode, opt.debug);
		img_opt.blocks_per_checksum = opt.blocks_per_checksum;
		img_opt.reseed_checksum = opt.reseed_checksum;

		cs_size = img_opt.checksum_size;
		cs_reseed = img_opt.reseed_checksum;

		log_mesg(1, 0, 0, debug, "Initial image hdr - get Super Block from partition\n");
		log_mesg(0, 0, 1, debug, "Reading Super Block\n");

		/// get Super Block information from partition
		read_super_blocks(source, &fs_info);

		if (img_opt.checksum_mode != CSM_NONE && img_opt.blocks_per_checksum == 0) {

			const unsigned int buffer_capacity = opt.buffer_size > fs_info.block_size
				? opt.buffer_size / fs_info.block_size : 1; // in blocks

			img_opt.blocks_per_checksum = buffer_capacity;

		}
		log_mesg(1, 0, 0, debug, "%u blocks per checksum\n", img_opt.blocks_per_checksum);

		check_mem_size(fs_info, img_opt, opt);

		/// alloc a memory to store bitmap
		bitmap = pc_alloc_bitmap(fs_info.totalblock);
		if (bitmap == NULL) {
			log_mesg(0, 1, 1, debug, "%s, %i, not enough memory\n", __func__, __LINE__);
		}

		log_mesg(2, 0, 0, debug, "initial main bitmap pointer %p\n", bitmap);
		log_mesg(1, 0, 0, debug, "Initial image hdr - read bitmap table\n");

		/// read and check bitmap from partition
		log_mesg(0, 0, 1, debug, "Calculating bitmap... Please wait... \n");
		read_bitmap(source, fs_info, bitmap, pui);
		update_used_blocks_count(&fs_info, bitmap);

		log_mesg(2, 0, 0, debug, "check main bitmap pointer %p\n", bitmap);
		log_mesg(1, 0, 0, debug, "Writing super block and bitmap...\n");

		log_mesg(0, 0, 1, debug, "done!\n");

	}

	log_mesg(1, 0, 0, debug, "print image information\n");

	/// print option to log file
	if (debug)
		print_opt(opt);

	print_file_system_info(fs_info, opt);

	/**
	 * initial progress bar
	 */
	start = 0;				/// start number of progress bar
	stop = (fs_info.totalblock - fs_info.usedblocks);		/// get the end of progress number, only used block
	log_mesg(1, 0, 0, debug, "Initial Progress bar\n");
	/// Initial progress bar
	if (opt.no_block_detail)
		flag = NO_BLOCK_DETAIL;
	else
		flag = IO;
	progress_init(&prog, start, stop, fs_info.totalblock, flag, fs_info.block_size);
	copied = 0;				/// initial number is 0

	/**
	 * thread to print progress
	 */
	pres = pthread_create(&prog_thread, NULL, thread_update_pui, NULL);
	if(pres)
	    log_mesg(0, 1, 1, debug, "%s, %i, thread create error\n", __func__, __LINE__);


	/**
	 * start read and write data between source and destination
	 */
	if (1) {

		const unsigned long long blocks_total = fs_info.totalblock;
		const unsigned int block_size = fs_info.block_size;
		const unsigned int buffer_capacity = opt.buffer_size > block_size ? opt.buffer_size / block_size : 1; // in blocks
		unsigned char checksum[cs_size];
		unsigned int blocks_in_cs, blocks_per_cs, write_size;
		char *write_buffer;

		blocks_per_cs = img_opt.blocks_per_checksum;

		log_mesg(1, 0, 0, debug, "#\nBuffer capacity = %u, Blocks per cs = %u\n#\n", buffer_capacity, blocks_per_cs);

		write_size = cnv_blocks_to_bytes(0, buffer_capacity, block_size, &img_opt);

		write_buffer = (char*)calloc(block_size, sizeof(char));

		if (write_buffer == NULL) {
			log_mesg(0, 1, 1, debug, "%s, %i, not enough memory\n", __func__, __LINE__);
		}

		log_mesg(0, 0, 0, debug, "Total block %llu\n", blocks_total);

		/// start clone partition to image file
		log_mesg(1, 0, 0, debug, "start zerroing data...\n");

		blocks_in_cs = 0;
		init_checksum(img_opt.checksum_mode, checksum, debug);

		{
			/// scan bitmap
			unsigned long long i, blocks_skip, blocks_read;
			unsigned int cs_added = 0, write_offset = 0;
			off_t offset;

			/// read blocks
			for (block_id = 0; block_id < blocks_total; ++block_id) {
				if (!pc_test_bit(block_id, bitmap, fs_info.totalblock)) {
					offset = (off_t)(block_id * block_size);
					if (lseek(dfr, offset, SEEK_SET) == (off_t)-1)
						log_mesg(0, 1, 1, debug, "source seek ERROR:%s\n", strerror(errno));
					w_size = write_all(&dfr, write_buffer, block_size, &opt);
					if (w_size != block_size)
						log_mesg(0, 1, 1, debug, "write ERROR:%s\n", strerror(errno));
					copied += 1;
				}
			}
		}

		free(write_buffer);
	}

	done = 1;
	pres = pthread_join(prog_thread, &p_result);
	if(pres)
	    log_mesg(0, 1, 1, debug, "%s, %i, thread join error\n", __func__, __LINE__);
	update_pui(&prog, copied, block_id, done);
#ifndef CHKIMG
	sync_data(dfr, &opt);
#endif
	print_finish_info_zerofree(opt);

	/// close source
	close(dfr);
	/// free bitmp
	free(bitmap);
	close_pui(pui);
#ifndef CHKIMG
	fprintf(stderr, "Zerofree completed successfully.\n");
#else
	printf("Checked successfully.\n");
#endif
	if (opt.debug)
		close_log();
#ifdef MEMTRACE
	muntrace();
#endif
	return 0;      /// finish
}
